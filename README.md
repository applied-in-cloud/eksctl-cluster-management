## Terraform Script to Provision EKS Cluster
### Create or Update kubeconfig file -
```
aws eks update-kubeconfig --region <REGION_CODE> --name <CLUSTER_NAME>
```
### Template Snippet References -
- [EKS Optimized AMIs Reference](https://github.com/awslabs/amazon-eks-ami/blob/master/CHANGELOG.md)
- [EKS Managed Node Group - Example Terraform template](https://github.com/terraform-aws-modules/terraform-aws-eks/blob/master/examples/eks_managed_node_group/main.tf)
- [EKS Managed Node Group - Input/Output](https://registry.terraform.io/modules/terraform-aws-modules/eks/aws/latest/submodules/eks-managed-node-group)

### Generate Terraform plan into a output file
```
terraform plan -no-color > tfplan-1.txt
```
### Reference for EKS module and its allowed variables
https://github.com/terraform-aws-modules/terraform-aws-eks/tree/master/modules/eks-managed-node-group