module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.4.1"

  cluster_name    = var.cluster_name
  cluster_version = var.cluster_version

  cluster_endpoint_public_access = true

  cluster_addons = {
    coredns = {
      most_recent = true
    }
    kube-proxy = {
      most_recent = true
    }
    vpc-cni = {
      most_recent = true
    }
  }

  vpc_id     = module.vpc.vpc_id
  subnet_ids = module.vpc.private_subnets

  # Create OpenID Connect Provider for EKS to enable IRSA (IAM Role Service Account)
  enable_irsa = true

  cluster_enabled_log_types              = var.cluster_enabled_log_types
  cloudwatch_log_group_retention_in_days = var.cloudwatch_log_group_retention_in_days

  # EKS Managed Node Group(s)
  eks_managed_node_group_defaults = {
    instance_types = [var.instance_type]
    # Latest AMI Release version reference - https://github.com/awslabs/amazon-eks-ami/blob/master/CHANGELOG.md
    # Can be fixed to specific custom AMI version. e.g. ami_id = 1.24.7-20221112    
    #ami_id        = data.aws_ami.eks_default.image_id
    capacity_type = "ON_DEMAND"

    ebs_optimized           = true
    disable_api_termination = false
    enable_monitoring       = true

    iam_role_additional_policies = {
      CloudWatchAgentServerPolicy = "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
      #Additional_Policy          = aws_iam_policy.node_additional.arn
    }
  }

  # EKS Managed Node Group(s)
  eks_managed_node_groups = {
    node-group-1 = {
      #name = "ng-group-one" # Prefix for Node Group Name

      min_size     = 1
      desired_size = 1
      max_size     = 2

      # Worker Node Role
      create_iam_role = false
      iam_role_arn    = aws_iam_role.eks_worker_node_role.arn
      #launch_template_name = "ng-group-one-lt" # Prefix for Launch Template

      block_device_mappings = {
        xvda = {
          device_name = "/dev/xvda"
          ebs = {
            volume_size = 20
            volume_type = "gp2"
            #iops                  = 3000
            #throughput            = 150
            encrypted             = true
            delete_on_termination = true
          }
        }
      }
      /*
      tags = {
        ExtraTag = "EKS managed node group"
      }
      */
    }
  }
  /*
  cluster_tags = {
    # Extra tags added in the cluster in addition to tags already added to all resources
    cluster-management-team = "devopsteam@test.com"
  }
  */
  tags = {
    environment = "development"
    iaac-tool   = "terraform"
    contact     = "abc@test.com"
  }
}
/*
data "aws_ami" "eks_default" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amazon-eks-node-${var.cluster_version}-v*"]
  }
}
*/