/*
resource "aws_launch_template" "small_nodegroup_lt" {
  name_prefix            = "small-nodegroup-lt-"
  description            = "My default Launch Template"
  update_default_version = true

  block_device_mappings {
    device_name = "/dev/xvda"

    ebs {
      volume_size           = 20
      volume_type           = "gp2"
      delete_on_termination = true
      encrypted             = true
    }
  }

  ebs_optimized = true

  monitoring {
    enabled = true
  }

  network_interfaces {
    associate_public_ip_address = false
    delete_on_termination       = true
    #security_groups             = [module.eks.worker_security_group_id] # Security Groups attached to EC2
  }

  # Custom tags to launched EKS instances
  tag_specifications {
    resource_type = "instance"
    tags          = var.instance_tags
  }

  # Custom tags to Volumes of launched EKS instances
  tag_specifications {
    resource_type = "volume"
    tags          = var.default_tags
  }

  # Tag the LT itself
  tags = var.default_tags

  lifecycle {
    create_before_destroy = true
  }
}
*/