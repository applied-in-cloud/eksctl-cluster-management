## Replace the placeholders
#  ACCOUNT_ID
#  OIDC_ID

# IAM Policy for EKS Auto Scaler
resource "aws_iam_policy" "AmazonEKSClusterAutoscalerPolicy" {
  name        = "AmazonEKSClusterAutoscalerPolicy"
  path        = "/"
  description = "Amazon EKS - Cluster autoscaler policy"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "autoscaling:SetDesiredCapacity",
          "autoscaling:TerminateInstanceInAutoScalingGroup"
        ],
        "Resource" : "*",
        "Condition" : {
          "StringEquals" : {
            "aws:ResourceTag/k8s.io/cluster-autoscaler/cloud-in-bytes" : "owned"
          }
        }
      },
      {
        "Effect" : "Allow"
        "Action" : [
          "autoscaling:DescribeAutoScalingGroups",
          "autoscaling:DescribeAutoScalingInstances",
          "autoscaling:DescribeInstances",
          "autoscaling:DescribeLaunchConfigurations",
          "autoscaling:DescribeTags",
          "ec2:DescribeLaunchTemplateVersions",
          "ec2:DescribeInstanceTypes"
        ],
        "Resource" : "*"
      }
    ]
  })

  tags = var.default_tags
}

# IAM Role for EKS Auto Scaler
resource "aws_iam_role" "AmazonEKSClusterAutoscalerRole" {
  name        = "AmazonEKSClusterAutoscalerRole"
  description = "Amazon EKS - Cluster autoscaler role"
  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : "sts:AssumeRoleWithWebIdentity",
        "Principal" : {
          "Federated" : "arn:aws:iam::<ACCOUNT_ID>:oidc-provider/oidc.eks.us-east-1.amazonaws.com/id/<OIDC_ID>"
        },
        "Condition" : {
          "StringEquals" : {
            "oidc.eks.us-east-1.amazonaws.com/id/<OIDC_ID>:aud" : [
              "sts.amazonaws.com"
            ]
          }
        }
      }
    ]
  })
  managed_policy_arns   = [aws_iam_policy.AmazonEKSClusterAutoscalerPolicy.arn]
  force_detach_policies = true
  tags                  = var.default_tags
}
