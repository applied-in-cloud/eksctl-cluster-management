variable "cluster_name" {
  description = "Name of the EKS cluster"
  type        = string
  default     = "cloud-in-bytes"
}

variable "cluster_version" {
  description = "Kubernetes `<major>.<minor>` version to use for the EKS cluster (i.e.: `1.24`)"
  type        = string
  default     = "1.24"
}

variable "instance_type" {
  default = "t3.small"
  type    = string
}

variable "cluster_enabled_log_types" {
  description = "A list of the desired control plane logs to enable. For more information, refer https://docs.aws.amazon.com/eks/latest/userguide/control-plane-logs.html"
  type        = list(string)
  default     = ["audit", "api", "authenticator", "controllerManager", "scheduler"]
}

variable "cloudwatch_log_group_retention_in_days" {
  description = "Number of days to retain log events. Default retention - 90 days"
  type        = number
  default     = 30
}

variable "instance_tags" {
  type = map(any)
  default = {
    Name        = "EKS Workers"
    environment = "development"
    iaac-tool   = "terraform"
    contact     = "abc@test.com"
  }
}

variable "default_tags" {
  type = map(any)
  default = {
    environment = "development"
    iaac-tool   = "terraform"
    contact     = "abc@test.com"
  }
}
